<?php
    require_once("dao/dao.php");
    function obtenerProductos(){
        global $conn; //esto hace disponible la conexion dentro de esta funcíon
        $productos = array();
        $sqlstr = "SELECT * FROM productos;";
        $productos = obtenerRegistros($conn, $sqlstr);
        return $productos;
    }
?>